import pytz as pytz
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone


class Mailing(models.Model):
    date_start = models.DateTimeField(verbose_name='Начало рассылки')
    date_end = models.DateTimeField(verbose_name='Окончание рассылки')
    time_start = models.TimeField(verbose_name='Время начала отправки сообщения')
    time_end = models.TimeField(verbose_name='Время окончания отправки сообщения')
    text = models.TextField(max_length=255, verbose_name='Сообщение')
    tag = models.CharField(max_length=100, verbose_name='Поиск по тегу', blank=True)
    mobile_operator_code = models.CharField(verbose_name='Поиск по коду мобильного оператора',
                                            max_length=3, blank=True)

    @property
    def to_send(self):
        now = timezone.now()
        if self.date_start <= now <= self.date_end:
            return True
        else:
            return False

    def __str__(self):
        return f'Отправка {self.id} от {self.date_start}'

    class Meta:
        verbose_name = 'Отправка'
        verbose_name_plural = 'Отправки'


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_regex = RegexValidator(regex=r'^7\d{10}$',
                                 message="Номер телефона клиента в формате7XXXXXXXXXX (X - цифры от 0 до 9)")
    phone_number = models.CharField(verbose_name='Номер телефона', validators=[phone_regex], unique=True, max_length=11)
    mobile_operator_code = models.CharField(verbose_name='Код мобильного оператора', max_length=3, editable=False)
    tag = models.CharField(verbose_name='Тег', max_length=100, blank=True)
    timezone = models.CharField(verbose_name='Часовой пояс', max_length=32, choices=TIMEZONES, default='UTC')

    def save(self, *args, **kwargs):
        self.mobile_operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'Клиент {self.id} с номером {self.phone_number}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    SENT = "отправлено"
    NO_SENT = "не отправлено"

    STATUS_CHOICES = [
        (SENT, "Отправлено"),
        (NO_SENT, "Не отправлено"),
    ]

    time_create = models.DateTimeField(verbose_name='Время создания', auto_now_add=True)
    sending_status = models.CharField(verbose_name='Статус отправки', max_length=15, choices=STATUS_CHOICES)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='рассылка')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='клиент')

    def __str__(self):
        return f'Сообщение {self.id} с текстом {self.mailing} для {self.client}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'