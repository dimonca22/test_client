import requests
import pytz
import datetime
from dotenv import load_dotenv
from celery.utils.log import get_task_logger

from .models import *

logger = get_task_logger(__name__)

load_dotenv()
URL = " "
TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI5NTI4NzksImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkRpbW9uY2EifQ.p1tKbZ4_Uebg3RIvIjfoLgC4us2Qkp1tj_yNsQ41hgo'


@app.task(bind=True, retry_backoff=True)
def send_message(self, data, client_id, mailing_id, url=URL, token=TOKEN):
    mail = Mailing.objects.get(pk=mailing_id)
    client = Client.objects.get(pk=client_id)
    timezone = pytz.timezone(client.timezone)
    now = datetime.datetime.now(timezone)

    if mail.time_start <= now.time() <= mail.time_end:
        header = {
            'Авторизация': f'JWT {token}',
            'Тип содержимого': 'application/json'}
        try:
            requests.post(url=url + str(data['id']), headers=header, json=data)
        except requests.exceptions.RequestException as exc:
            logger.error(f"Сообщение, если: {data['id']} is error")
            raise self.retry(exc=exc)
        else:
            logger.info(f"Сообщение id: {data['id']}, Статус отправки:'Отправлено'")
            Message.objects.filter(pk=data['id']).update(sending_status='Отправлено')
    else:
        time = 24 - (int(now.time().strftime('%H:%M:%S')[:2]) -
                     int(mail.time_start.strftime('%H:%M:%S')[:2]))
        logger.info(f"Message id: {data['id']}, "
                    f"Текущее время не предназначено для отправки сообщения,"
                    f"перезапуск задачи после {60 * 60 * time} секунд")
        return self.retry(countdown=60 * 60 * time)
